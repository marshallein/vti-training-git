
import java.util.EmptyStackException;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Marshallein
 */
public class Student {

    private int id;
    private String name;
    private int age;

    public Student() {
    }

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void addStudent(List stdlist){
        stdlist.add(new Student(2,"an"));
        return;
    }
    
     public void addStudent2(List stdlist){
        return;
    }
    
    public void DeleteAllStudent(List stdlist){
        stdlist.removeAll(stdlist);
        return;
    }
    
    public void UpdateStudent(List stdlist){
        return;
    }
    
    public void returnStudentWithId(List stdlist){
        return;
    }
    
    public void addSomething(){
        
    }
    
    public void addSomething(int age){
        
    }

    public void addSomething(int age,int name){
        
    }
}
