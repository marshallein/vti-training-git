/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Marshallein
 */
public class StudentProgram extends Student{

    private int program;
    private String program_name; 
    
    public StudentProgram() {
    }

    public StudentProgram(int program, String program_name, int id, String name) {
        super(id, name);
        this.program = program;
        this.program_name = program_name;
    }

    public StudentProgram(int program, String program_name) {
        this.program = program;
        this.program_name = program_name;
    }
    
    
}
