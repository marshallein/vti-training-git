/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author FPTSHOP
 */
public class InforDAO extends DBContext{
    public Infor getInforByName(String txt) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM [Infor].[dbo].[infor] where name like ?";
            con = getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, txt);
            rs = ps.executeQuery();
            while (rs.next()) {
                Infor i = new Infor(rs.getString("name"),
                        rs.getString("email"));
                return i;
            }
        } catch (ClassNotFoundException | SQLException e) {
            throw e;
        } finally {
            closeRs(rs);
            closePs(ps);
            closeCon(con);
        }
        return null;
    }
}
